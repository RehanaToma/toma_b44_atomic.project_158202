-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2017 at 06:53 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toma_atomic_projects`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`, `soft_deleted`) VALUES
(1, 'Toma', '1991-12-11', 'No'),
(2, 'Nadia', '1996-04-05', 'No'),
(3, 'Nowrin', '1999-09-09', 'No'),
(4, 'Noah', '2000-10-10', 'No'),
(5, 'Xyz', '1992-08-07', 'Yes'),
(6, 'Abc', '1998-07-08', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Book2', 'Author2', 'No'),
(2, 'Book3', 'Author3', 'No'),
(3, 'Book4', 'Author4', 'No'),
(4, 'Book5', 'Author5', 'No'),
(5, 'Book6', 'Author6', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `soft_deleted`) VALUES
(1, 'Dhaka', 'No'),
(2, 'Chittagong', 'No'),
(3, 'Rajshahi', 'Yes'),
(4, 'Khulna', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'Toma', 'toma.rehana@gmail.com', 'No'),
(2, 'Nadia', 'nadia@yahoo.com', 'No'),
(3, 'Nowrin', 'nowrin@gmail.com', 'No'),
(4, 'Person1', 'person1@yahoo.com', 'Yes'),
(5, 'Person2', 'person2@gmail.com', 'Yes'),
(6, 'Sultana', 'sadiasultana012@yahoo.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_summary`
--

CREATE TABLE `organization_summary` (
  `id` int(11) NOT NULL,
  `org_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `owner_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `founding_time` date NOT NULL,
  `mission` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization_summary`
--

INSERT INTO `organization_summary` (`id`, `org_name`, `type`, `owner_name`, `founding_time`, `mission`, `soft_deleted`) VALUES
(1, 'Org. A', 'Small Business', 'Mr. A', '2012-01-27', 'Spread Handicrafts', 'No'),
(2, 'Org. B', 'NGO', 'Mr. B', '2015-11-01', 'Alleviate Poverty', 'Yes'),
(3, 'Org. C', 'Educational Institution', 'Miss C', '2012-12-12', 'Educating People', 'Yes'),
(4, 'Org. D', 'Software Development Firm', 'Miss D', '2016-12-02', 'Prepare Excellent Softwares for diverse applications', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_pic`, `soft_deleted`) VALUES
(13, 'Person1', '1-Blue_Fairy.jpg', 'No'),
(14, 'Person2', 'cute (5).jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `students_genders`
--

CREATE TABLE `students_genders` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students_genders`
--

INSERT INTO `students_genders` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'Toma', 'Female', 'No'),
(2, 'Nadia', 'Female', 'No'),
(3, 'Nowrin', 'Female', 'Yes'),
(4, 'Noah', 'Male', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `students_hobbies`
--

CREATE TABLE `students_hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students_hobbies`
--

INSERT INTO `students_hobbies` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Toma', 'Travelling', 'No'),
(2, 'Nadia', 'Reading', 'No'),
(3, 'Nowrin', 'Photography', 'No'),
(5, 'Noah', 'Reading', 'No'),
(13, 'Jannah', 'Travelling', 'No'),
(14, 'Tasmiah', 'Photography', 'No'),
(15, 'hu', 'Photography', 'Yes'),
(16, 'Tasmiah', 'Reading', 'No'),
(17, 'Student1', 'Reading', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_summary`
--
ALTER TABLE `organization_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_genders`
--
ALTER TABLE `students_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_hobbies`
--
ALTER TABLE `students_hobbies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `organization_summary`
--
ALTER TABLE `organization_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `students_genders`
--
ALTER TABLE `students_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `students_hobbies`
--
ALTER TABLE `students_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
