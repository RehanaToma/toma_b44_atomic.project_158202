<?php
namespace App\Hobbies;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO, PDOException;
class Hobbies extends DB
{
    private $id;
    private $studentName;
    private $hobby;

    public function setData($postData)
    {

        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }

        if (array_key_exists('studentName', $postData)) {
            $this->studentName = $postData['studentName'];
        }

        if (array_key_exists('hobby', $postData)) {
            $this->hobby = $postData['hobby'];
        }

    }


    public function store()
    {

        $arrData = array($this->studentName, $this->hobby);

        $sql = "INSERT into students_hobbies(name,hobby) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('create.php');

    }

    public function index(){

        $sql = "select * from students_hobbies where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from students_hobbies where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from students_hobbies where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}