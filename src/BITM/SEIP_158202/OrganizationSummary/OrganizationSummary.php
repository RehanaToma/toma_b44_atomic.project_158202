<?php
namespace App\OrganizationSummary;

use App\Message\Message;
use App\Utility\Utility;

use App\Model\Database as DB;
use PDO, PDOException;
class OrganizationSummary extends DB
{
    private $id;
    private $org_name;
    private $type;
    private $owner_name;
    private $founding_time;
    private $mission;


    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('orgName',$postData)){
            $this->org_name = $postData['orgName'];
        }

        if(array_key_exists('orgType',$postData)){
            $this->type = $postData['orgType'];
        }

        if(array_key_exists('ownerName',$postData)){
            $this->owner_name = $postData['ownerName'];
        }

        if(array_key_exists('foundationTime',$postData)){
            $this->founding_time= $postData['foundationTime'];
        }

        if(array_key_exists('orgMission',$postData)){
            $this->mission = $postData['orgMission'];
        }

    }


    public function store(){

        $arrData = array($this->org_name,$this->type,$this->owner_name,$this->founding_time,$this->mission);

        $sql = "INSERT into organization_summary(org_name,type,owner_name,founding_time,mission) VALUES(?,?,?,?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }



    public function index(){

        $sql = "select * from organization_summary where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from organization_summary where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from organization_summary where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}
