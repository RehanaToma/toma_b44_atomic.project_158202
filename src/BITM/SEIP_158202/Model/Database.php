<?php
namespace App\Model;

use PDO;
use PDOException;
class Database
{
    public $DBH;


    public function __construct()
    {

        try {

            $this->DBH = new PDO('mysql:host=localhost;dbname=toma_atomic_projects', "root", "");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

        } catch (PDOException $error) {

            print "Database Error!: " . $error->getMessage() . "<br/>";
            die();
        }


    }

}

