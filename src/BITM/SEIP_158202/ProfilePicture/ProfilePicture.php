<?php
namespace App\ProfilePicture;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO, PDOException;
class ProfilePicture extends DB
{
    private $id;
    private $tmpName;
    private $personName;
    private $profilePicture;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('personName',$postData)) {
            $this->personName = $postData['personName'];

        }

        if(array_key_exists('profilePicture',$postData)){
            if(array_key_exists("personName",$postData)){
                $this->profilePicture = $postData['profilePicture']['name'];
            }
        }
        if(array_key_exists('profilePicture',$postData)){
            if(array_key_exists("personName",$postData)){
                $this->tmpName = $postData['profilePicture']['tmp_name'];
            }
        }
    }

    public function moveFile() {
        move_uploaded_file($this->tmpName, "C:\\xampp\\htdocs\\Toma_158202_B44_Atomic_Projects\\views\\SEIP_158202\\ProfilePicture\\Uploaded Files\\".$this->profilePicture);
    }

    public function store() {

        $arrData = array($this->personName,$this->profilePicture);

        $sql = "INSERT INTO profile_picture (name,profile_pic) VALUES (?,?)";

        $statement = $this->DBH->prepare($sql);

        $result = $statement->execute($arrData);

        if ($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "select * from profile_picture where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from profile_picture where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from profile_picture where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}